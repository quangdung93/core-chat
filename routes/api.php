<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\ZaloController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth/login', [AuthController::class, 'login']);

// Route::group(['middleware' => 'jwt.auth'], function () {
//     Route::post('auth/register', [AuthController::class, 'register']);
//     Route::get('/info', [ZaloController::class, 'getUserInfo']);
//     Route::get('/quote-message', [ZaloController::class, 'getQuoteMessageAction']);
//     Route::post('/send-message', [ZaloController::class, 'sendMessageAction']);
//     Route::get('/messages-by-user', [ZaloController::class, 'getListRecentChatByUserIdAction']);
//     Route::get('/messages-recent', [ZaloController::class, 'getListRecentChatAction']);
// });

Route::post('auth/register', [AuthController::class, 'register']);
Route::get('/user-info', [ZaloController::class, 'getUserInfoAction']);
Route::get('/follows', [ZaloController::class, 'getFollowersAction']);
Route::get('/info', [ZaloController::class, 'getInfoOaAction']);
Route::get('/quota-message', [ZaloController::class, 'getQuotaMessageAction']);
Route::post('/send-message', [ZaloController::class, 'sendMessageAction']);
Route::post('/react-message', [ZaloController::class, 'sendReactMessageAction']);
Route::get('/messages-by-user', [ZaloController::class, 'getListRecentChatByUserIdAction']);
Route::get('/messages-recent', [ZaloController::class, 'getListRecentChatAction']);
Route::get('/list-rooms', [RoomController::class, 'getListRooms']);
Route::post('/messages', [MessageController::class, 'getMessages']);
Route::post('/upload-image', [UploadController::class, 'uploadImageAction']);
Route::post('/upload-file', [UploadController::class, 'uploadFileAction']);

Route::get('/zalo', [ZaloController::class, 'index']);