<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZaloController;
use App\Http\Controllers\WebhookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'Chathub Core';
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::get('/download/{file}',[ZaloController::class, 'download']);
Route::post('/webhook', [WebhookController::class, 'webhook']);
Route::get('/chat', [ZaloController::class, 'chat']);