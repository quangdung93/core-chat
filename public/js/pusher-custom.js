// PUSHER Notification
Pusher.logToConsole = false;
var pusher = new Pusher(MIX_PUSHER_APP_KEY, {
    cluster: PUSHER_APP_CLUSTER
});

var channel = pusher.subscribe('user.' + USER_ID);
    channel.bind('App\\Events\\ChatEvent', function(data) {
        console.log('socket-in', data);
});
