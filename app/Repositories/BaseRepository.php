<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements RepositoryInterface
{
    //model muốn tương tác
    protected $model;

    //khởi tạo
    public function __construct()
    {
        $this->setModel();
    }

    public static function instance()
    {
        return (new static());
    }

    //lấy model tương ứng
    abstract public function getModel();

    public function getTable() {
        return $this->model->getTable();
    }

    public function model(){
        return $this->model;
    }

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make($this->getModel());

        return $this;
    }

    public function observe($class)
    {
        return $this->model->observe($class);
    }

    public function getAll()
    {
        return $this->model->get();
    }

    /**
     * @ param int $id
     * @ return Model|null
     */
    public function find($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    /**
     * @ param array $attributes
     * @ return bool
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes); // newInstance
    }

    /**
     * @ param int $id
     * @ param array $attributes
     * @ return bool
     */
    public function update($id, $attributes = [])
    {
        $result = $this->find($id);
        if ($result) {
            $result->update($attributes);
            return $result;
        }

        return false;
    }

    /**
     * @ param int $id
     * @ return bool
     */
    public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }

        return false;
    }
}
