<?php
namespace App\Repositories\ZaloApp;


interface ZaloAppRepositoryInterface
{
    /**
     * Get Zalo APP 
     * @return mixed
     */
    public function getZaloApp($appId);

}
