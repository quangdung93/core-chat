<?php
namespace App\Repositories\ZaloApp;

use App\Models\ZaloApp;
use App\Repositories\BaseRepository;

/**
 * Class TokenRepository
 */
class ZaloAppRepository extends BaseRepository implements ZaloAppRepositoryInterface
{
    public function getModel()
    {
        return ZaloApp::class;
    }

    public function getZaloApp($appId)
    {
        return $this->model->where('app_id', $appId)->where('status', 1)->first();
    }
}
