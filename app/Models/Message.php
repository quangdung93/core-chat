<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $guarded = [];

    public function author(){
        return $this->hasOne(User::class, 'id', 'author_id');
    }

    public function reply(){
        return $this->belongsTo(self::class, 'reply_message_id', 'id');
    }

    
}
