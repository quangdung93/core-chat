<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';

    protected $guarded = [];

    public function lastMessage(){
        return $this->hasOne(Message::class, 'id', 'last_message');
    }

    public function lastAuthor(){
        return $this->hasOne(User::class, 'id', 'last_author');
    }

    public function author(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function scopeWithHas($query, $relation, $callback){
        return $query->whereHas($relation, $callback)->with([$relation => $callback]);
    }
}
