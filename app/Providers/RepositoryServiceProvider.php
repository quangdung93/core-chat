<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\ZaloApp\ZaloAppRepository;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ZaloAppRepositoryInterface::class, ZaloAppRepository::class);
    }
}