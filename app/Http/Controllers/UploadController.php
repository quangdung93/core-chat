<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\Services\ZaloOAService;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use App\Exceptions\ApiHandleException;
use Illuminate\Support\Facades\Storage;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class UploadController extends Controller
{   
    protected $zaloAppRepo;

    public function __construct(ZaloAppRepositoryInterface $zaloAppRepo){
        $this->zaloAppRepo = $zaloAppRepo;
    }

    public function uploadImageAction(Request $request)
    {
        // try {
            $files = $request->images;
            $userId = $request->user_id;
            $path = 'images/'.$userId;

            $userObj = User::where('id', $userId)->first();

            if(!$userObj){
                return $this->responseJson(CODE_ERROR, null, 'Lỗi dữ liệu', 'Bạn chưa chọn người chat', 400);;
            }

            $uploadService = new UploadService();
            $zaloApp = $this->zaloAppRepo->getZaloApp(ZALO_APP_ID);
            $zaloOAService = new ZaloOAService($zaloApp);

            $result = $uploadService->uploadImage($files, $path);

            $uploadToZalo = [];
            if($result){
                foreach($result as $key => $file){ 
                    $uploadToZalo[$key] = $zaloOAService->uploadImage($file['original']);
                    $uploadToZalo[$key]['path'] = $file['original'];
                }
            }

            return $this->responseJson(CODE_SUCCESS, $uploadToZalo);
        // }
        // catch (ApiHandleException $e) {
        //     return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        // }
    }

    public function uploadFileAction(Request $request)
    {
        // try {
            $files = $request->file('files');
            $userId = $request->user_id;
            $path = 'files/'.$userId;

            $userObj = User::where('id', $userId)->first();

            if(!$userObj){
                return $this->responseJson(CODE_ERROR, null, 'Lỗi dữ liệu', 'Bạn chưa chọn người chat', 400);;
            }

            $uploadService = new UploadService();
            $zaloApp = $this->zaloAppRepo->getZaloApp(ZALO_APP_ID);
            $zaloOAService = new ZaloOAService($zaloApp);

            $result = $uploadService->uploadFile($files, $path);

            $uploadToZalo = [];
            if($result){
                foreach($result as $key => $file){ 
                    $objFile = explode('.', $file['original']);
                    $fileExtension = end($objFile);

                    if(in_array($fileExtension, ['xls', 'xlsx'])){ //File excel
                        return $this->responseJson(2, ['path' => $file['original']]);
                    }

                    $uploadToZalo[$key] = $zaloOAService->uploadFileApi($file['original']);
                    $uploadToZalo[$key]['path'] = $file['original'];
                }
            }

            return $this->responseJson(CODE_SUCCESS, $uploadToZalo);
        // }
        // catch (ApiHandleException $e) {
        //     return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        // }
    }
}
