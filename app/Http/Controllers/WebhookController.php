<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use App\Models\Message;
use App\Models\ZaloApp;
use Illuminate\Http\Request;
use App\Services\ChatService;
use App\Services\ZaloOAService;
use Illuminate\Support\Facades\Hash;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class WebhookController extends Controller
{   
    protected $zaloAppRepo;

    public function __construct(ZaloAppRepositoryInterface $zaloAppRepo){
        $this->zaloAppRepo = $zaloAppRepo;
    }

    public function webhook(Request $request){
        $header = $request->header('X-ZEvent-Signature');
        $data = $request->all();

        if(!$data || !$header){
            return false;
        }

        $zaloApp = $this->zaloAppRepo->getZaloApp($data['app_id']);

        zalo_logging('[Webhook] '. $data['event_name'] . ':', [
            'header' => $header,
            'data' => $data
        ]);

        if (!$zaloApp) {
            zalo_logging(__CLASS__.' - [Webhook] app_id not found:', [], 'warning');
            return false;
        }

        $hash = "mac=" . hash("sha256", $data['app_id'] . json_encode($data, JSON_UNESCAPED_UNICODE) . $data['timestamp'] . trim($zaloApp->oa_secret_key));

        $isEcho = $this->isWebhookOaSend($data['event_name']) ? true : false;

        if($isEcho){
            $zaloUserId = $data['recipient']['id'];
            $oaId = $data['sender']['id'];
        }
        else{
            $zaloUserId = $data['sender']['id'];
            $oaId = $data['recipient']['id'];
        }

        //React Message
        if($data['event_name'] == 'user_reacted_message'){
            $roomId = $this->handleReactedMessage($data);

            if($roomId){
                $chatService = new ChatService();

                $dataSocket = [
                    'reacted' => $data['message']['react_icon'],
                    'room_id' => $roomId
                ];

                $chatService->pushSocket($dataSocket);
            }

            return true;
        }

        $zaloService = new ZaloOAService($zaloApp);

        $user = User::where('user_social_id', $zaloUserId)->with('room')->first();

        //Nếu người dùng đã tồn tại
        if($user){
            // Check nếu là tin nhắn duplicate từ OA thì bỏ qua
            if($isEcho){
                $messageEcho = Message::where('room_id', $user->room->id)->orderBy('created_at', 'DESC')->first();

                if($messageEcho && $messageEcho->content == $data['message']['text']){
                    return true;
                }
            }

            $message = $this->handleSendMessage($user->room->id, $user->id, $data);
            return true;
        }

        $userZaloInfo = $zaloService->getProfile($zaloUserId);

        if($userZaloInfo && isset($userZaloInfo['user_id'])){
            $userZaloName = $userZaloInfo['display_name'];
            $avatar = $userZaloInfo['avatars']['240'];
        }
        else{
            $recentChatByUser = $zaloService->getListRecentChatByUserId($zaloUserId, 0, 1);
            if(count($recentChatByUser) > 0){
                $recentChatByUser = $recentChatByUser[0];
                $userZaloName = $isEcho ? $recentChatByUser['to_display_name'] : $recentChatByUser['from_display_name']; 
                $avatar = $isEcho ? $recentChatByUser['to_avatar'] : $recentChatByUser['from_avatar'];
            }else{
                $userZaloName = 'Ẩn danh'; 
                $avatar = '';
            }
        }

        $dataUser = [
            'name' => $userZaloName,
            'user_name' => $zaloUserId,
            'user_social_id' => $zaloUserId,
            'email' => $zaloUserId.'@gmail.com',
            'source' => 'zalo',
            'avatar' => $avatar,
            'password' => Hash::make($zaloUserId),
        ];

        $createUser = User::create($dataUser);

        if(!$createUser){
            return;
        }

        //Upload avatar
        $imagePath = $this->downloadImage($createUser->id, $avatar);

        $dataRoom = [
            'user_id' => $createUser->id,
            'message_unseen' => 1,
            'source' => 'zalo',
            'channel_id' => $zaloApp->id,
            'last_author' => $createUser->id,
        ];

        $createRoom = Room::create($dataRoom);

        if(!$createRoom){
            return;
        }

        $message = $this->handleSendMessage($createRoom->id, $createUser->id, $data);

        if($message){
            $createRoom->update(['last_message' => $message->id]);
        }

        return true;

        // if (strcmp($hash, $header) == 0) { // data verified
        //     zalo_logging('[Webhook] data verified', [$hash, $header]);
            
        //     $chatService = new ChatService();
        //     $chatService->pushSocket($data);

        //     return true;
        // }
        // else{
        //     zalo_logging('[Webhook] Data not verified', [$hash, $header]);
        //     return false;
        // }
    }

    public function handleReactedMessage($dataWebhook){
        if(isset($dataWebhook['message']['msg_id'])){
            $message = Message::where('social_message_id', $dataWebhook['message']['msg_id'])->first();
            $reactEmoji = explode(',', $message->react);
            $newReact = array_merge($reactEmoji, [$dataWebhook['message']['react_icon']]);

            if(count($newReact) > 0){
                $message->react = implode(',', $newReact);
                $message->save();
                return $message->room_id;
            }
        }

        return false;
    }

    public function handleSendMessage($roomId, $userId, $dataWebhook) {

        switch ($dataWebhook['event_name']) {
            case 'user_send_text': //Text

                // Nếu là tin nhắn reply
                if(isset($dataWebhook['message']['quote_msg_id'])){
                    $messageReply = Message::where('social_message_id', $dataWebhook['message']['quote_msg_id'])->first();
                    $quoteMessageId = $messageReply ? $messageReply->id : null;
                }

                $dataMessage = [
                    'room_id' => $roomId,
                    'author_id' => $userId,
                    'social_message_id' => $dataWebhook['message']['msg_id'],
                    'content' => $dataWebhook['message']['text'],
                    'reply_message_id' => $quoteMessageId ?? null,
                    'type' => 'text',
                ];
        
                return $this->pushMessage($dataMessage);
            case 'oa_send_text': //OA Send Text
                $dataMessage = [
                    'room_id' => $roomId,
                    'author_id' => ADMIN_ID,
                    'social_message_id' => $dataWebhook['message']['msg_id'],
                    'content' => $dataWebhook['message']['text'],
                    'type' => 'text',
                ];
        
                return $this->pushMessage($dataMessage);
            case 'user_send_image': //Image
                foreach ($dataWebhook['message']['attachments'] as $item) {
                    $imageUrl = $item['payload']['url'];
                    $imagePath = $this->downloadImage($userId, $imageUrl);

                    $dataMessage = [
                        'room_id' => $roomId,
                        'author_id' => $userId,
                        'social_message_id' => $dataWebhook['message']['msg_id'],
                        'content' => $imagePath,
                        'type' => 'image',
                    ];

                    return $this->pushMessage($dataMessage);
                }

                break;
            case 'user_send_gif': //Gif
                foreach ($dataWebhook['message']['attachments'] as $item) {
                    $imageUrl = $item['payload']['url'];
                    $imagePath = $this->downloadImage($userId, $imageUrl);

                    $dataMessage = [
                        'room_id' => $roomId,
                        'author_id' => $userId,
                        'social_message_id' => $dataWebhook['message']['msg_id'],
                        'content' => $imagePath,
                        'type' => 'image',
                    ];

                    return $this->pushMessage($dataMessage);
                }

                break;
            case 'user_send_file': //File
                foreach ($dataWebhook['message']['attachments'] as $item) {
                    $fileUrl = $item['payload']['url'];
                    $fileName = $item['payload']['name'];
                    $fileSize = $item['payload']['size'];
                    $fileType = $item['payload']['type'];
                    $filePath = $this->downloadFile($userId, $fileUrl, $fileName, $fileSize, $fileType);

                    $dataMessage = [
                        'room_id' => $roomId,
                        'author_id' => $userId,
                        'social_message_id' => $dataWebhook['message']['msg_id'],
                        'content' => $filePath,
                        'type' => 'file',
                    ];

                    return $this->pushMessage($dataMessage);
                }

                break;
            case 'user_send_sticker': //Sticker
                $dataMessage = [
                    'room_id' => $roomId,
                    'author_id' => $userId,
                    'social_message_id' => $dataWebhook['message']['msg_id'],
                    'content' => $dataWebhook['message']['attachments'][0]['payload']['url'],
                    'type' => 'sticker',
                ];
        
                return $this->pushMessage($dataMessage);
            case 'user_send_location': //Location
                $latitude = $dataWebhook['message']['attachments'][0]['payload']['coordinates']['latitude'];
                $longitude = $dataWebhook['message']['attachments'][0]['payload']['coordinates']['longitude'];
                
                $dataMessage = [
                    'room_id' => $roomId,
                    'author_id' => $userId,
                    'social_message_id' => $dataWebhook['message']['msg_id'],
                    'content' => "Người dùng gửi vị trí: latitude: ${latitude}, longitude: ${longitude}",
                    'type' => 'text',
                ];
        
                return $this->pushMessage($dataMessage);
            case 'user_send_link': //Link
                $dataMessage = [
                    'room_id' => $roomId,
                    'author_id' => $userId,
                    'social_message_id' => $dataWebhook['message']['msg_id'],
                    'content' => $dataWebhook['message']['attachments'][0]['payload']['url'],
                    'type' => 'link',
                ];
        
                return $this->pushMessage($dataMessage);
            default:
                # code...
                break;
        }
    }

    public function pushMessage($dataMessage){
        $message = Message::create($dataMessage);

        $roomId = $dataMessage['room_id'];

        $room = Room::where('id', $roomId)->first();
        $room->last_author = $dataMessage['author_id'];
        $room->last_message = $message->id;
        $room->message_unseen = 1;
        $room->save();

        $message = $message->load('author');

        if($message){
            $chatService = new ChatService();
            $chatService->pushSocket($message);
            return $message;
        }

        return null;
    }

    public function isWebhookOaSend($action) {
        $listWebhookOa = ['oa_send_text', 'oa_send_image', 'oa_send_file', 'oa_send_sticker'];
        if(in_array($action, $listWebhookOa)){
            return true;
        }
    
        return false;
    }

}
