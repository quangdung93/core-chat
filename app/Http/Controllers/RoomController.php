<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use App\Services\ZaloOAService;
use App\Exceptions\ApiHandleException;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class RoomController extends Controller
{   
    protected $zaloAppRepo;

    public function __construct(ZaloAppRepositoryInterface $zaloAppRepo){
        $this->zaloAppRepo = $zaloAppRepo;
    }

    private function _getService(){
        $zaloApp = $this->zaloAppRepo->getZaloApp(ZALO_APP_ID);
        $zaloService = new ZaloOAService($zaloApp);
        return $zaloService;
    }

    public function getListRooms(Request $request){
        try {
            $limit = $request->limit ?? 30;
            $offset = $request->offset ?? 0;

            $query = Room::with('lastMessage', 'lastAuthor');

            if($request->keyword){
                $keyword = $request->keyword;
                $query->withHas("author", function($q) use($keyword){
                    $q->where('name', 'like', '%'.$keyword.'%');
                });
            }
            else{
                $query->with('author');
            }

            $query->orderBy('updated_at', 'DESC')->skip($offset)->take($limit);
            $rooms = $query->get();

            return $this->responseJson(CODE_SUCCESS, $rooms);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }
}
