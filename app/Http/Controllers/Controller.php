<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\Traits\ResponseFormatTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use ResponseFormatTrait;
    use UploadTrait;
}
