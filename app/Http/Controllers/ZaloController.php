<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\User;
use App\Models\Message;
use App\Models\ZaloApp;
use Illuminate\Http\Request;
use App\Services\ChatService;
use App\Services\ZaloOAService;
use App\Exceptions\ApiHandleException;
use App\Http\Requests\SendMessageRequest;
use App\Http\Requests\QuoteMessageRequest;
use App\Http\Requests\MessageByUserRequest;
use App\Http\Requests\MessageRecentRequest;
use App\Http\Requests\SendReactMessageRequest;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class ZaloController extends Controller
{   
    protected $zaloAppRepo;

    public function __construct(ZaloAppRepositoryInterface $zaloAppRepo){
        $this->zaloAppRepo = $zaloAppRepo;
    }

    public function chat(){
        return view('chat');
    }

    private function _getService(){
        $zaloApp = $this->zaloAppRepo->getZaloApp(ZALO_APP_ID);
        $zaloService = new ZaloOAService($zaloApp);
        return $zaloService;
    }

    public function sendMessageAction(SendMessageRequest $request){
        try {
            $zaloService = $this->_getService();

            $userObj = User::where('id', $request->user_id)->first();

            if(!$userObj){
                return $this->responseJson(CODE_ERROR, null, 'Lỗi dữ liệu', 'user_id không tồn tại', 400);;
            }

            $roomObj = Room::where('id', $request->room_id)->first();

            if(!$roomObj){
                return $this->responseJson(CODE_ERROR, null, 'Lỗi dữ liệu', 'room_id không tồn tại', 400);;
            }

            $result = $zaloService->sendMessage($userObj, $roomObj, $request);

            return $this->responseJson(CODE_SUCCESS, $result);

        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function sendReactMessageAction(SendReactMessageRequest $request){
        try {
            $zaloService = $this->_getService();

            $userObj = User::where('id', $request->user_id)->first();

            if(!$userObj){
                return $this->responseJson(CODE_ERROR, null, 'Lỗi dữ liệu', 'user_id không tồn tại', 400);;
            }

            $messageObj = Message::where('id', $request->react_message_id)->first();

            $result = $zaloService->sendReactMessage($userObj, $messageObj, $request);

            //Cập nhật lại icon vào message
            if($result && isset($result['message_id'])){
                $react = explode(',', $messageObj->react);
                $newReact = array_merge($react, [$request->react_icon]);

                if(count($newReact) > 0){
                    $messageObj->react = implode(',', $newReact);
                    $messageObj->save();
                }
            }

            return $this->responseJson(CODE_SUCCESS, $result);

        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function getListRecentChatAction(){
        try {
            $zaloService = $this->_getService();

            $result = $zaloService->getListRecentChat();

            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function getListRecentChatByUserIdAction(MessageByUserRequest $request){
        try {
            $zaloService = $this->_getService();

            $result = $zaloService->getListRecentChatByUserId($request->user_id);

            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function getQuotaMessageAction(){
        try {
            $zaloService = $this->_getService();

            $result = $zaloService->getQuotaMessage();

            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function getUserInfoAction(Request $request){
        $zaloService = $this->_getService();

        try {
            $zaloService = $this->_getService();
            $result = $zaloService->getProfile($request->user_id);
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
        // $result = $zaloService->sendMessage($userId, $message);
        // $result = $zaloService->getFollowers();
        // $result = $zaloService->getListRecentChat();
        // $result = $zaloService->getListRecentChatByUserId(6542666606033968121);
        // $result = $zaloService->getInfoOA();
    }

    public function getFollowersAction(Request $request){
        $zaloService = $this->_getService();

        try {
            $zaloService = $this->_getService();
            $result = $zaloService->getFollowers();
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function getInfoOaAction(Request $request){
        $zaloService = $this->_getService();

        try {
            $zaloService = $this->_getService();
            $result = $zaloService->getInfoOA();
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }

    public function download($file){
        $filePath = public_path('storage/'.$file);
        return response()->download($filePath);
    }

}
