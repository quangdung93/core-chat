<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Exceptions\ApiHandleException;
use App\Repositories\ZaloApp\ZaloAppRepositoryInterface;

class MessageController extends Controller
{   
    protected $zaloAppRepo;

    public function __construct(ZaloAppRepositoryInterface $zaloAppRepo){
        $this->zaloAppRepo = $zaloAppRepo;
    }

    public function getMessages(Request $request){
        try {
            $limit = $request->limit ?? 30;
            $offset = $request->offset ?? 0;
            $messages = Message::where('room_id', $request->room_id)
                            ->with('author', 'reply')
                            ->orderBy('id', 'DESC')
                            ->skip($offset)
                            ->take($limit)
                            ->get()->reverse()->toArray();

            $result = [
                'total' => count($messages),
                'messages' => array_values($messages)
            ];

            //Update seen message
            $room = Room::where('id', $request->room_id)->first();
            $room->timestamps = false;
            $room->message_unseen = 0;
            $room->save();

            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        }
    }
}
