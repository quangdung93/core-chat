<?php

namespace App\Http\Requests;

use App\Exceptions\ApiHandleException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class MessageByUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User ID không được trống'
        ];
    }

    protected function failedValidation(Validator $validator)
    {  
        $error = $validator->errors()->first();
        throw new ApiHandleException($error, $error);
    }
}
