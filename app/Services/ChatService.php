<?php

namespace App\Services;

use Pusher\Pusher;
use App\Models\User;
use App\Api\ApiAbstract;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ChatNotification;

class ChatService extends ApiAbstract
{
    protected $pusher;
    
    public function __construct()
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );

        //config 
        $this->pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'), 
            $options
        );
    }

    public function pushSocket($message)
    {   
        // try{
            //user receive notify
            $user = User::find(1); 
            $data = [
                'message' => $message,
            ];
            
            $user->notify(new ChatNotification($data));
            $this->pusher->trigger('user.'.$user->id, 'App\\Events\\ChatEvent', $data);
            
        // }
        // catch(\Exception $e){
        //     return null;
        // }
    }

}
