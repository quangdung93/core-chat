<?php

namespace App\Services;

use App\Models\User;
use App\Models\Message;
use App\Api\ApiAbstract;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ApiHandleException;

class ZaloOAService extends ApiAbstract
{
    public $zaloApp = null;

    public function __construct($zaloApp = null){
        if($zaloApp){
            $this->zaloApp = $zaloApp;
        }
    }

    public function sendMessage($userObj, $roomObj, $request){
        $chatService = new ChatService();

        $type = $request->type;
        $message = $request->message;
        $quoteMessageId = $request->quote_message_id ?? null;
        $odooId = $request->odoo_id ?? null;
        $odooName = $request->odoo_name ?? null;

        $dataMessage = [
            'room_id' => $roomObj->id,
            'reply_message_id' => $quoteMessageId,
            'admin_id' => $odooId,
            'admin_name' => $odooName,
            'author_id' => ADMIN_ID,
            'content' => $message,
            'type' => $type,
        ];

        if($type == 'image' || $type == 'file'){
            $dataMessage['content'] = $request->file_path ?? null;
        }

        if(filter_var($request->message, FILTER_VALIDATE_URL)){ //is URL
            $dataMessage['type'] = 'link';
        }

        $messageObj = Message::create($dataMessage);


        $quoteMessage = Message::where('id', $quoteMessageId)->first();
        $quoteMessageSocialId = $quoteMessage ? $quoteMessage->social_message_id : null;

        if($messageObj){
            $roomObj->update(['last_message' => $messageObj->id, 'last_author' => $userObj->id]);
            $chatService->pushSocket($message);

            $this->sendMessageToSocial($type, $userObj, $roomObj, $messageObj, $quoteMessageSocialId, $message);

            return $message;
        }

        return null;
    }
    
    /**
     * Gửi tin nhắn
     *
     * @param  mixed $userId
     * @param  mixed $message
     * @return void
     */
    public function sendMessageToSocial($type, $userObj, $roomObj, $messageObj, $quoteMessageSocialId, $message){
        $messageData = [];

        switch ($type) {
            case 'text':
                $messageData['text'] = $messageObj->content;
                if($quoteMessageSocialId){
                    $messageData['quote_message_id'] = $quoteMessageSocialId;
                }

                break;
            case 'image':
                //Upload image

                $messageData['text'] = "";
                $messageData['attachment'] = [
                    'type' => 'template',
                    'payload' => [
                        'template_type' => 'media',
                        'elements' => [
                            [
                                'media_type' => 'image',
                                'attachment_id' => $message, //Image URL
                            ]
                        ],
                    ],
                ];
                break;
            case 'file':
                $messageData['attachment'] = [
                    'type' => 'file',
                    'payload' => [
                        'token' => $message, //token file
                    ],
                ];
                break;
            
            default:
                # code...
                break;
        }

        $params = [
            'recipient' => ['user_id' => $userObj->user_social_id],
            'message' => $messageData
        ];

        $response = $this->_callApi('POST', 'v2.0/oa/message', $params);

        //Người dùng chưa follow OA
        if($response && isset($response['error']) && $response['error'] == ApiHandleException::USER_NOT_FOLLOW_OA){
            $messageRecent = Message::where(['author_id' => $userObj->id, 'room_id' => $roomObj->id])
                                        ->orderBy('id', 'DESC')
                                        ->first();
                                        
            if($messageRecent && $messageRecent->social_message_id){
                $paramsNotFollowOA = [
                    'recipient' => ['message_id' => $messageRecent->social_message_id],
                    'message' => $messageData
                ];

                $response = $this->_callApi('POST', 'v2.0/oa/message', $paramsNotFollowOA);
            }
        }

        //Update social_message_id
        if($response && isset($response['message_id']) && $messageObj){
            $messageObj->update(['social_message_id' => $response['message_id']]);
        }

        return $response;
    }

    /**
     * Upload image
     *
     * @return void
     */
    public function uploadImage($file){
        $url = 'v2.0/oa/upload/image';
        $params = [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => fopen($file, 'rb'),
                ]
            ]
        ];

        $params['headers'] = [
            "access_token" => $this->_getAccessToken()
        ];

        $client = $this->_getClient();

        $response = $client->request('POST', $url, $params);

        $result = json_decode($response->getBody(), true);

        if ($response->getStatusCode() == 200){
            // Success
            if (isset($result['error']) && $result['error'] == self::CODE_SUCCESS) {

                //Log success
                if(self::LOG_SUCCESS){
                    zalo_logging('[API] Success:', ['api' => $url,'request' => $params,'response' => $result]);
                }

                return $result['data'];
            }

            // Error API
            zalo_logging('[API] Failed:', ['request' => $params,'response' => $result], 'warning');
            throw new ApiHandleException(
                ApiHandleException::ZALO_ERROR_MESSAGE[$result['error']], 
                $result['message'] ?? null,
                $result['error']
            );
        }

        return $result;
    }

    /**
     * Upload File
     *
     * @return void
     */
    public function uploadFileApi($file){
        $url = 'v2.0/oa/upload/file';
        $params = [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => fopen($file, 'rb'),
                ]
            ]
        ];

        $params['headers'] = [
            "access_token" => $this->_getAccessToken()
        ];

        $client = $this->_getClient();

        $response = $client->request('POST', $url, $params);

        $result = json_decode($response->getBody(), true);

        if ($response->getStatusCode() == 200){
            // Success
            if (isset($result['error']) && $result['error'] == self::CODE_SUCCESS) {

                //Log success
                if(self::LOG_SUCCESS){
                    zalo_logging('[API] Success:', ['api' => $url,'request' => $params,'response' => $result]);
                }

                return $result['data'];
            }

            // Error API
            zalo_logging('[API] Failed:', ['request' => $params,'response' => $result], 'warning');
            throw new ApiHandleException(
                ApiHandleException::ZALO_ERROR_MESSAGE[$result['error']], 
                $result['message'] ?? null,
                $result['error']
            );
        }

        return $result;
    }

    /**
     * Thả biểu tượng cảm xúc tin nhắn
     *
     * @return void
     */
    public function sendReactMessage($userObj, $messageObj, $request){
        $params = [
            'recipient' => ['user_id' => $userObj->user_social_id],
            'sender_action' => [
                'react_icon' => $request->react_icon,
                'react_message_id' => $messageObj->social_message_id
            ]
        ];

        return $this->_callApi('POST', 'v2.0/oa/message', $params);
    }
    
    /**
     * Lấy hạn mức tin nhắn
     *
     * @return void
     */
    public function getQuotaMessage(){
        return $this->_callApi('POST', 'v2.0/oa/quota/message');
    }
    
    /**
     * Lấy thông tin OA
     *
     * @return void
     */
    public function getInfoOA(){
        return $this->_callApi('GET', 'v2.0/oa/getoa');
    }
    
    /**
     * Lấy danh sách người quan tâm
     *
     * @return void
     */
    public function getFollowers(){
        $params = [
            "offset" => 10,
            "count" => 50,
            // "tag_name" => "Khách hàng Q1"
        ];

        return $this->_callApi('GET', 'v2.0/oa/getfollowers', $params);
    }

    /**
     * Lấy thông tin người quan tâm
     *
     * @return void
     */
    public function getProfile($userId){
        $params = [
            "user_id" => $userId
        ];

        return $this->_callApi('GET', 'v2.0/oa/getprofile', $params);
    }

    /**
     * Lấy danh sách chat gần đây
     *
     * @return void
     */
    public function getListRecentChat($offset = 0, $limit = 10){
        if($limit > 10){
            $limit = 10;
        }

        $params = [
            "offset" => $offset,
            "count" => $limit,
        ];

        return $this->_callApi('GET', 'v2.0/oa/listrecentchat', $params);
    }

    /**
     * Lấy danh sách chat gần đây theo user_id
     *
     * @return void
     */
    public function getListRecentChatByUserId($userId, $offset = 0, $limit = 10){
        if($limit > 10){
            $limit = 10;
        }
        
        $params = [
            'user_id' => $userId,
            "offset" => $offset,
            "count" => $limit,
        ];

        $result = $this->_callApi('GET', 'v2.0/oa/conversation', $params);

        return array_reverse($result);
    }

}
