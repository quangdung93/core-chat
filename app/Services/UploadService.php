<?php

namespace App\Services;

use App\Models\User;
use App\Api\ApiAbstract;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UploadService extends ApiAbstract
{   
    public function __construct()
    {

    }

    public function uploadImage($files, $path){
        $sizes = [256, 512];

        if(!$files){
            return null;
        }

        $listResult = [];
        foreach($files as $file){
            //get filename with extension
            $filenamewithextension = $file->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $filename = Str::slug($filename);
            //get file extension
            $extension = $file->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '_' . uniqid() . '.' . $extension;

            Storage::put('/public/' . $path . '/' . $filenametostore, fopen($file, 'r+'));

            $result['original'] = asset('storage/' . $path . '/' . $filenametostore);

            foreach ($sizes as $size) {
                Storage::put('/public/' . $path . '/'.$size.'/' . $filenametostore, fopen($file, 'r+'));

                //Resize image big thumbnail
                $thumbnailBigpath = storage_path('/app/public/' . $path . '/'.$size.'/' . $filenametostore);
                $img = Image::make($thumbnailBigpath)->resize($size, $size);
                $img->save($thumbnailBigpath);
                $result[$size] = asset('storage/' . $path . '/'.$size.'/' . $filenametostore);
            }

            $listResult[] = $result;
        }

        return $listResult;
    }


    public function uploadFile($files, $path){
        if(!$files){
            return null;
        }

        $listResult = [];
        foreach($files as $file){
            //get filename with extension
            $filenamewithextension = $file->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $filename = Str::slug($filename);
            //get file extension
            $extension = $file->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '_' . uniqid() . '.' . $extension;

            Storage::put('/public/' . $path . '/' . $filenametostore, fopen($file, 'r+'));

            $result['original'] = asset('storage/' . $path . '/' . $filenametostore);

            $listResult[] = $result;
        }

        return $listResult;
    }

}
