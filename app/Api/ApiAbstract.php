<?php
namespace App\Api;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Models\ZaloApp;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ApiHandleException;

abstract class ApiAbstract
{
    const LOG_SUCCESS = true;
    const CODE_SUCCESS = 0;
    const ZALO_BASE_URI = "https://openapi.zalo.me";
    const REFRESH_TOKEN_EXPIRES_IN = 30; // 30 days
    const URL_ACCESS_TOKEN = "https://oauth.zaloapp.com/v4/oa/access_token";
    protected $count = 0;

    protected function _callApi($method = "POST", $url, $data = []){
        try {
            $params['headers'] = [
                "access_token" => $this->_getAccessToken(),
                'Content-Type' => 'application/json',
            ];
    
            if(!empty($data)){
                $params['json'] = $data;
            }
    
            $client = $this->_getClient();
    
            $response = $client->request($method, $url, $params);
        }
        catch (GuzzleException $ex) {
            // Guzzle Error
            zalo_logging('[API] GuzzleException Failed:', ['message' => $ex->getMessage()], 'error');
            throw new ApiHandleException(ApiHandleException::GUZZLE_ERROR_MESSAGE, $ex->getMessage(), ApiHandleException::GUZZLE_ERROR);
        }

        if ($response->getStatusCode() == 200){
            $result = json_decode($response->getBody(), true);

            // Gửi tin nhắn chưa follow OA
            if($result['error'] == ApiHandleException::USER_NOT_FOLLOW_OA){
                return $result;
            }

            // Success
            if (isset($result['error']) && $result['error'] == self::CODE_SUCCESS) {

                //Log success
                if(self::LOG_SUCCESS){
                    zalo_logging('[API] Success:', ['api' => $url,'request' => $data,'response' => $result]);
                }

                return $result['data'];
            }

            // Access Token Expired
            if(isset($result['error']) && $result['error'] == ApiHandleException::ACCESS_TOKEN_INVALID){
                zalo_logging('[API] Access Token expired:', ['api' => $url,'request' => $data,'response' => $result]);

                if($this->count >= 2){
                    throw new ApiHandleException(
                        ApiHandleException::ZALO_ERROR_MESSAGE[ApiHandleException::ACCESS_TOKEN_INVALID], "",
                        ApiHandleException::ACCESS_TOKEN_INVALID
                    );
                }

                $this->_handleGetAccessToken();
                $this->count++;

                //Recall api
                return $this->_callApi($method, $url, $data);
            }

            // Error API
            zalo_logging('[API] Failed:', ['request' => $data,'response' => $result], 'warning');
            throw new ApiHandleException(
                ApiHandleException::ZALO_ERROR_MESSAGE[$result['error']], 
                $result['message'] ?? null,
                $result['error']
            );
        }

        // HTTP Error
        zalo_logging('[API] Failed:', ['api' => $url,'request' => $data,'response' => $response->getStatusCode()], 'error');
        throw new ApiHandleException(ApiHandleException::ERROR_HTTP_MESSAGE, "", ApiHandleException::ERROR_HTTP);
    }

    protected function _getAccessToken(){
        if (!$this->zaloApp) {
            zalo_logging(__CLASS__.' - Zalo App not set', [], 'warning');
            return false;
        }

        if(!$this->zaloApp->access_token || $this->_hasExpired()){
            return $this->_handleGetAccessToken();
        }

        return $this->zaloApp->access_token;
    }

    protected function _handleGetAccessToken(){
        $response = $this->_callApiGetAccessToken();

        if(empty($response['access_token'])){
            zalo_logging('[API] Get Access Token Failed:', $response, 'error');
            return false;
        }

        zalo_logging('[API] Get Access Token:', $response);

        $this->_updateAccessToken($response);

        return $response['access_token'];
    }

    protected function _callApiGetAccessToken(){
        //Header
        $params['headers'] = [
            "secret_key" => $this->zaloApp->app_secret,
            "Content-Type" => "application/x-www-form-urlencoded"
        ];

        //Body
        $params['form_params'] = [
            "refresh_token" => $this->zaloApp->refresh_token,
            "app_id" => $this->zaloApp->app_id,
            "grant_type" => "refresh_token"
        ];

        $resp = (new Client)->request('POST', self::URL_ACCESS_TOKEN, $params);
        $response = json_decode($resp->getBody()->getContents(), true);

        return $response;
    }

    protected function _updateAccessToken($token){
        $data = [
            'access_token' => $token['access_token'],
            'refresh_token' => $token['refresh_token'],
            'expires_in' => Carbon::parse(Carbon::now())->addSeconds($token['expires_in']),
            'refresh_expires_in' => Carbon::parse(Carbon::now())->addDays(self::REFRESH_TOKEN_EXPIRES_IN)
        ];

        $object = ZaloApp::where(['id' => $this->zaloApp->id])->first();
        $update = $object->update($data);

        if($update){
            zalo_logging('[ZaloApp] Update success', $data);
            $this->zaloApp = $object;
        }
        else{
            zalo_logging('[ZaloApp] Update failed', $data, 'warning');
        }

        return $update;
    }

    protected function _hasExpired() {
        if($this->zaloApp->expires_in < Carbon::now()) {
            return true;
        }

        return false;
    }

    protected function _getClient(){
        return new Client([
            'http_errors' => false,
            'base_uri' => self::ZALO_BASE_URI
        ]);
    }
}
