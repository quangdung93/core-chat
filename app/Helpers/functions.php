<?php 

use Illuminate\Support\Facades\Log;

const CODE_SUCCESS = 0;
const CODE_ERROR = 1;
const ADMIN_ID = 1;
const ZALO_APP_ID = '697891624756171834';

if(!function_exists('zalo_logging')){
    function zalo_logging($title = 'Zalo API', $context = [], $level = 'info'){

        if(!is_array($context)){
            $context = (array)$context;
        }

        switch ($level) {
            case 'info':
                Log::info($title, $context);
                break;
            case 'warning':
                Log::warning($title, $context);
                break;
            case 'error':
                Log::error($title, $context);
                break;
            default:
                break;
        }
    }
}