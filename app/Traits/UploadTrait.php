<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait UploadTrait
{
    protected function downloadImage($userId, $url){
        $contents = file_get_contents($url);
        $name = substr($url, strrpos($url, '/') + 1);
        $path = 'images/'.$userId.'/'.uniqid().'_'.$name;
        Storage::put('/public/'.$path, $contents);

        return $path;
    }

    protected function downloadFile($userId, $fileUrl, $fileName, $fileSize, $fileType){
        $contents = file_get_contents($fileUrl);
        $path = 'files/'.$userId.'/'.uniqid().'_'.$fileName;
        Storage::put('/public/'.$path, $contents);

        return $path;
    }
}