<?php
namespace App\Exceptions;

class ApiHandleException extends \Exception
{
    const CODE_MANY_REQUEST = -32;
    const UPLOAD_FAILED = -200;
    const PARAMS_INVALID = -201;
    const MAC_INVALID = -202;
    const OA_DELETED = -204;
    const OA_NOT_FOUND = -205;
    const OA_NOT_REGISTER_3RD_PARTY = -207;
    const OA_NOT_SECRET_KEY = -208;
    const API_NOT_SUPPORT = -209;
    const PARAMS_EXCEEDING = -210;
    const QUOTA_OVER = -211;
    const OA_NOT_REGISTER_API = -212;
    const USER_NOT_FOLLOW_OA = -213;
    const POST_NOT_HANDLE = -214;
    const APP_ID_INVALID = -215;
    const ACCESS_TOKEN_INVALID = -216;
    const USER_BLOCK_MESSAGE = -217;
    const RECEIVE_QUOTA_OVER = -218;
    const OA_UNAUTHORIZED = -221;
    const OA_NOT_PAID_PACKAGE = -224;
    const OA_OUT_OF_REPLY = -305;
    const OUT_USER_REPLY = -311;
    const APP_NOT_CONNECT_ZBA = -320;
    const ZBA_OUT_OF_MONEY = -321;

    const ZALO_ERROR_MESSAGE = [
        self::CODE_MANY_REQUEST => "Vượt quá giới hạn request/phút. Chi tiết tại đây",
        self::PARAMS_INVALID => "Tham số không hợp lệ",
        self::UPLOAD_FAILED => "Upload thất bại",
        self::MAC_INVALID => "Mac không hợp lệ",
        self::OA_DELETED => "Official Account đã bị xóa",
        self::OA_NOT_FOUND => "Official Account không tồn tại",
        self::OA_NOT_REGISTER_3RD_PARTY => "Official Account chưa đăng ký làm 3rd party",
        self::OA_NOT_SECRET_KEY => "Official Account chưa có secret key",
        self::API_NOT_SUPPORT => "Api này không được hỗ trợ",
        self::PARAMS_EXCEEDING => "Tham số vượt quá giới hạn cho phép",
        self::PARAMS_EXCEEDING => "Hết quota",
        self::OA_NOT_REGISTER_API => "Official Account chưa đăng ký api này",
        self::USER_NOT_FOLLOW_OA => "Người dùng chưa quan tâm Official Account",
        self::POST_NOT_HANDLE => "Bài viết đang được xử lý",
        self::APP_ID_INVALID => "App id không hợp lệ",
        self::ACCESS_TOKEN_INVALID => "Access token không hợp lệ",
        self::USER_BLOCK_MESSAGE => "Người dùng đã chặn tin mời quan tâm",
        self::RECEIVE_QUOTA_OVER => "Hết quota nhận",
        self::OA_UNAUTHORIZED => "Tài khoản Official Account chưa chứng thực",
        self::OA_NOT_PAID_PACKAGE => "Official Account chưa mua gói dịch vụ. Chi tiết tại đây",
        self::OA_OUT_OF_REPLY => "Official Account chỉ có thể trả lời tin từ người dùng trong 48 giờ",
        self::OUT_USER_REPLY => "Hết lượt tin trả lời người dùng",
        self::APP_NOT_CONNECT_ZBA => "Ứng dụng của bạn cần kết nối với Zalo Business Account để sử dụng tính năng trả phí.",
        self::ZBA_OUT_OF_MONEY => "Zalo Business Account liên kết với App đã hết tiền hoặc không thể thực hiện trả phí."
    ];

    const GUZZLE_ERROR = 502;
    const GUZZLE_ERROR_MESSAGE = "Không kết nối được server";
    const ERROR_HTTP = 400;
    const ERROR_HTTP_MESSAGE = "Yêu cầu không hợp lệ";

    protected $subMessage;
    protected $errorCode;

    public function __construct($errorMessage, $subMessage = null, $errorCode = null){
        $this->subMessage = $subMessage;
        $this->errorCode = $errorCode;
        parent::__construct($errorMessage);
    }

    public function getSubMessage(){
        return $this->subMessage;
    }

    public function getErrorCode(){
        return $this->errorCode;
    }
}