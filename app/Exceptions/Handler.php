<?php

namespace App\Exceptions;

use Throwable;
use App\Traits\ResponseFormatTrait;
use App\Exceptions\ApiHandleException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    use ResponseFormatTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (ApiHandleException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage(), $e->getSubMessage(), $e->getErrorCode());
        });

        $this->renderable(function (UnauthorizedHttpException $e) {
            return $this->responseJson(CODE_ERROR, null, $e->getMessage());
        });
    }
}
