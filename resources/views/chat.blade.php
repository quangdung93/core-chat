<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Epoints Zalo Chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/js/bootstrap.bundle.min.js"></script>
    <style type="text/css">
        
        .chat-online {
            color: #34ce57;
        }

        .sub-message{
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 1;
            text-overflow: ellipsis;
            overflow: hidden;
            display: -webkit-box;
        }
        
        .chat-offline {
            color: #e4606d
        }
        
        .chat-messages {
            display: flex;
            flex-direction: column;
            max-height: 800px;
            overflow-y: scroll
        }
        
        .chat-message-left,
        .chat-message-right {
            display: flex;
            flex-shrink: 0
        }
        
        .chat-message-left {
            margin-right: auto
        }
        
        .chat-message-right {
            flex-direction: row-reverse;
            margin-left: auto
        }
        .py-3 {
            padding-top: 1rem!important;
            padding-bottom: 1rem!important;
        }
        .px-4 {
            padding-right: 1.5rem!important;
            padding-left: 1.5rem!important;
        }
        .flex-grow-0 {
            flex-grow: 0!important;
        }
        .border-top {
            border-top: 1px solid #dee2e6!important;
        }
    </style>
    <script type="text/javascript">
        var URL_MAIN = '{{ asset('') }}';
        var MIX_PUSHER_APP_KEY = '{{env("MIX_PUSHER_APP_KEY")}}';
        var PUSHER_APP_CLUSTER = '{{env("PUSHER_APP_CLUSTER")}}';
        var USER_ID = 1;
    </script>
</head>
<body>
<main class="content">
    <div class="container p-5">

		<h1 class="h3 mb-3">HITA Zalo Chat</h1>

		<div class="card">
			<div class="row g-0">
				<div class="col-12 col-lg-5 col-xl-3 border-right">

					<div class="px-4 d-none d-md-block">
						<div class="d-flex align-items-center">
							<div class="flex-grow-1">
								<input type="text" class="form-control my-3" placeholder="Search...">
							</div>
						</div>
					</div>

					<div class="message-recent"></div>

					<hr class="d-block d-lg-none mt-1 mb-0">
				</div>
				<div class="col-12 col-lg-7 col-xl-9">
					<div class="py-2 px-4 border-bottom d-none d-lg-block">
						<div class="d-flex align-items-center py-1 info-user">
							{{-- <div>
								<button class="btn btn-primary btn-lg mr-1 px-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone feather-lg"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg></button>
								<button class="btn btn-info btn-lg mr-1 px-3 d-none d-md-inline-block"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video feather-lg"><polygon points="23 7 16 12 23 17 23 7"></polygon><rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect></svg></button>
								<button class="btn btn-light border btn-lg px-3"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal feather-lg"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg></button>
							</div> --}}
						</div>
					</div>

					<div class="position-relative" style="min-height: 625px">
						<div class="chat-messages p-4"></div>
					</div>

					<div class="flex-grow-0 py-3 px-4 border-top">
                        <form id="frm-message">
                            <div class="input-group">
                                <input type="text" class="form-control mr-3" id="message" placeholder="Nhập tin nhắn...">
                                <button type="submit" class="btn btn-primary">Gửi</button>
                            </div>
                        </form>
					</div>

				</div>
			</div>
		</div>
	</div>
</main>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/pusher.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/pusher-custom.js')}}"></script>
<script type="text/javascript">
    const ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvemFsby1hcGkudGVzdFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY1ODM4MzI3MSwiZXhwIjoxNjU4NDY5NjcxLCJuYmYiOjE2NTgzODMyNzEsImp0aSI6Ild2QjJhQjZTYjU5RFRqUDUiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.bKHuln3q--a3Am6dHL1H5sb5Yu3SHb1d1ukEDj0Ekg8";
    const USER_ID_DUNGVQ = '6542666606033968121';
    const APP_ID = '3803877482057167724';
    $(function(){
        loadMessageByUser(USER_ID_DUNGVQ);
        loadMessageRecent();

        $('#frm-message').on('submit', function(e){
            e.preventDefault();

            let message = $('#message').val();
            let user_id = $('.user-info-block').data('id');

            if(!message || !user_id){
                return;
            }

            $.ajax({
                type: 'POST',
                url: 'api/send-message',
                data: {
                    user_id: user_id,
                    app_id: `${APP_ID}`,
                    message: message,
                },
                headers:{         
                    'Authorization' : `Bearer ${ACCESS_TOKEN}`,
                },
                success: function (response, status, xhr) {
                    if(response.error == 0){
                        loadMessageRecent();
                        loadMessageByUser(user_id);
                    }
                }
            });
        });

        $(document).on('click', '.list-group-item-action', function(e){
            e.preventDefault();
            let user_id = $(this).data('id');
            loadMessageByUser(user_id);
        });

        function loadMessageByUser(user_id){
            $.ajax({
                type: 'GET',
                url: 'api/messages-by-user',
                data: {app_id: `${APP_ID}`, user_id: `${user_id}`},
                headers:{         
                    'Authorization' : `Bearer ${ACCESS_TOKEN}`,
                },
                success: function (response, status, xhr) {
                    if(response.error == 0){
                        let html = renderMessageChatByUser(response.data);
                        $('.chat-messages').html(html[0]);
                        $('.info-user').html(html[1]);
                    }
                }
            });
        }

        function loadMessageRecent(){
            $.ajax({
                type: 'GET',
                url: 'api/messages-recent',
                data: {app_id: `${APP_ID}`},
                headers:{         
                    'Authorization' : `Bearer ${ACCESS_TOKEN}`,
                },
                success: function (response, status, xhr) {
                    if(response.error == 0){
                        let html = renderMessageRecent(response.data);
                        $('.message-recent').html(html);
                    }
                }
            });
        }

        function renderMessageChatByUser(messages){
            let html = html_info = '';
            let flag = false;
            for (const key in messages) {
                const message = messages[key];
                const time = new Date(message.time).toLocaleTimeString("vi-VN");
                const class_content = message.src == 0 ? 'chat-message-right' : 'chat-message-left';

                if(flag == false && message.src == 1){
                    html_info = `<div class="position-relative user-info-block" data-id="${message.from_id}">
                        <img src="${message.from_avatar}" class="rounded-circle mr-1" alt="" width="40" height="40">
                    </div>
                    <div class="flex-grow-1 pl-3">
                        <strong>${message.from_display_name}</strong>
                    </div>`;

                    flag = true;
                }

                let message_content = '';
                if(message.type == 'photo' || message.type == 'sticker'){    
                    message_content = `<img style="max-width:200px;height:auto" src="${message.url}" alt="">`;
                }
                else{
                    message_content = message.message;
                }

                html += `<div class="${class_content} pb-4">
                    <div>
                        <img src="${message.from_avatar}" class="rounded-circle mr-1" alt="Chris Wood" width="40" height="40">
                        <div class="text-muted small text-nowrap mt-2">${time}</div>
                    </div>
                    <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
                        <div class="font-weight-bold mb-1">${message.from_display_name}</div>
                        ${message_content}
                    </div>
                </div>`;
            }

            return [html, html_info];
        }

        function renderMessageRecent(messages){
            let html = '';
            for (const key in messages) {
                const message = messages[key];
                let name = message.src == 0 ? 'Hita' : message.to_display_name;
                let sub_message = '';

                if(message.type == 'text'){
                    sub_message = `<div class="small sub-message"><span class="fas fa-circle chat-online"></span> ${name}: ${message.message}</div>`;
                }
                // <div class="badge bg-success float-right">5</div>
                html += `<a href="#" class="list-group-item list-group-item-action border-0" data-id="${message.src == 1 ? message.from_id : message.to_id}">
                    <div class="d-flex align-items-start">
                        <img src="${message.src == 1 ? message.from_avatar : message.to_avatar }" class="rounded-circle mr-1" alt="" width="40" height="40">
                        <div class="flex-grow-1 ml-3">
                            ${message.src == 1 ? message.from_display_name : message.to_display_name}
                            ${sub_message}
                        </div>
                    </div>
                </a>`;
            }

            return html;
        }

    });
</script>
</body>
</html>